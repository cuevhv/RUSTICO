## RUSTICO: data sets
Benchmark data sets used for the evaluation of the performance of RUSTICO.

#### [1) TB-roses-v1](TB-roses-v1/)
The data set TB-roses-v1 contains 35 images of rose bushes, provided with manually segmented ground truth images of the rose stems. Images are taken in the [TrimBot2020 garden](http://trimbot2020.webhosting.rug.nl/) in Wageningen.

Please refer at the following paper for evaluation and results:

[N.Strisciuglio, G.Azzopardi, N.Petkov, Brain-inspired robust delineation operator, BDCV Workshop, European Conference on Computer Vision 2018](https://arxiv.org/abs/1811.10240)

Bibtex:

	@InProceedings{StrisciuglioBDCV2018,
	author="Strisciuglio, Nicola and Azzopardi, George and Petkov, Nicolai",
	editor="Leal-Taix{\'e}, Laura and Roth, Stefan",
	title="Brain-Inspired Robust Delineation Operator",
	booktitle="Computer Vision -- ECCV 2018 Workshops",
	year="2019",
	publisher="Springer International Publishing",
	address="Cham",
	pages="555--565"
	}



#### 2) TB-roses-v2
The data set TB-roses-v2 contains 319 images of rose bushes, provided with manually segmented ground truth images of the rose stems. Images are taken in the [TrimBot2020 garden](http://trimbot2020.webhosting.rug.nl/) in Wageningen.

The data set is, at the moment, private. It can be made available on request.

#### 3) Rose stems
This data set contains 8 images of rose bushes with ground truth of the rose stems. It is an initial version of the TB-roses data sets.
We provide pixel-wise ground truth for evaluation of segmentation (center image below), and the center-line (skeleton) of the stems for detection (right image below). 

<img src="./TB-roses/images/01.jpg" height="150" />
<img src="./TB-roses/gt_seg/01.png" height="150" />
<img src="./TB-roses/gt_skl/01.png" height="150" />

### Road and river images
Aerial images of roads and riversgit git  were collected form
[this website](http://www-sop.inria.fr/members/Florent.Lafarge/benchmark/line-network_extraction/line-networks.html), which also reports the results achieved by existing methods.

The images and ground truths are:

<img src="./road/road.bmp" height="120" />
<img src="./road/road_GT.bmp" height="120" />
<img src="./river/river.bmp" height="120" />
<img src="./river/river_GT.bmp" height="120" />


#### Retinal fundus images
We used images from the following data sets.  

__STARE__  
Download __[here](http://cecas.clemson.edu/~ahoover/stare/probing/index.html)__

_A. Hoover, V. Kouznetsova and M. Goldbaum, "Locating Blood Vessels in Retinal Images by Piece-wise Threhsold Probing of a Matched Filter Response", IEEE Transactions on Medical Imaging , vol. 19 no. 3, pp. 203-210, March 2000._

__DRIVE__  
Download __[here](https://www.isi.uu.nl/Research/Databases/DRIVE/)__  

_J.J. Staal, M.D. Abramoff, M. Niemeijer, M.A. Viergever, B. van Ginneken, "Ridge based vessel segmentation in color images of the retina", IEEE Transactions on Medical Imaging, 2004, vol. 23, pp. 501-509._

__CHASE_DB1__  
Download __[here](https://blogs.kingston.ac.uk/retinal/chasedb1/)__

_Owen, C.G., Rudnicka, A.R., Mullen, R., Barman, S.A., Monekosso, D., Whincup, P.H., Ng, J. and Paterson, C. (2009) Measuring retinal vessel tortuosity in 10-year-old children: validation of the Computer-Assisted Image Analysis of the Retina (CAIAR) program. Investigative Ophthalmology & Visual Science, 50(5), pp. 2004-2010._

__HRF__  
Download __[here](https://www5.cs.fau.de/research/data/fundus-images/)__

_Jan Odstrcilik, Radim Kolar, Attila Budai, Joachim Hornegger, Jiri Jan, Jiri Gazarek, Tomas Kubena, Pavel Cernosek, Ondrej Svoboda, Elli Angelopoulou, Retinal vessel segmentation by improved matched filtering: evaluation on a new high-resolution fundus image database, IET Image Processing, Volume 7, Issue 4, June 2013, pp.373-383._

#### Road cracks
* __CrackTree206__: 206 RGB images of asphalt crack, with ground truth
* __Crack_ivc__: 5 RGB images of asphalt crack, with ground truth
* __Crack_PV14__: 14 laser range images of asphalt crack, with ground truth


The data sets are available from the __[website of Dr. Qin Zou](https://sites.google.com/site/qinzoucn/)__

#### Synthetic noisy lines
To be published: set of synthetic images used to evaluate RUSTICO


