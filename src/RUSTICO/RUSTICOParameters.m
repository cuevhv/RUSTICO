function params = RUSTICOParameters()
% @version 1.1
% @author Nicola Strisciuglio (www.nicolastrisciuglio@rug.nl)
%
% Configuration of RUSTICO

% Use hashtable
params.ht                             = 1;

% The radii list of concentric circles
params.COSFIRE.rholist                = 0:2:8;

% Minimum distance between dominant contours lying on the same concentric circle
params.COSFIRE.eta                    = 150 * pi/180;

% Threshold parameter used to suppress the input filters responses that are less than a
% fraction t1 of the maximum
params.COSFIRE.t1                     = 0;

% Threshold parameter used to select the channels of input filters that
% produce a response larger than a fraction t2 of the maximum
params.COSFIRE.t2                     = 0.4;

% Parameters of the Gaussian function used to blur the input filter
% responses. sigma = sigma0 + alpha*rho_i
params.COSFIRE.sigma0                 = 3/6;
params.COSFIRE.alpha                  = 0.8/6;

% Parameters used for the computation of the weighted geometric mean. 

% mintupleweight is the weight assigned to the peripherial contour parts
params.COSFIRE.mintupleweight         = 0.5; % not-used in this implementation
params.COSFIRE.outputfunction         = 'geometricmean'; %'geometricmean'; % geometricmean OR weightedgeometricmean 'min'
params.COSFIRE.blurringfunction       = 'max'; %max or sum

% Weights are computed from a 1D Gaussian function. weightingsigma is the
% standard deviation of this Guassian function (not-used for RUSTICO)
params.COSFIRE.weightingsigma         = sqrt(-max(params.COSFIRE.rholist)^2/(2*log(params.COSFIRE.mintupleweight)));

% Threshold parameter used to suppress the responsesof the operator
% that are less than a fraction t3 of the maximum response.
params.COSFIRE.t3                     = 0; % ReLU on the output responses

% % Parameters of some geometric invariances
numoriens = 12;
params.invariance.rotation.psilist    = 0:pi/numoriens:(pi)-(pi/numoriens);
params.invariance.scale.upsilonlist   = 2.^((0)./2); % 0: no scale

% Reflection invariance about the y-axis. 0 = do not use, 1 = use.
params.invariance.reflection          = 0;

% Minimum distance allowed between detected keypoints. If the distance
% between any two pairs of detected keypoints is less than
% params.distance.mindistance then we keep only the stronger one.
params.detection.mindistance          = 8;

params.inputfilter.name                     = 'DoG';    
params.inputfilter.DoG.polaritylist         = [1];
params.inputfilter.DoG.sigmalist            = 2.4;
params.inputfilter.DoG.sigmaratio           = 0.5;
params.inputfilter.DoG.halfwaverect         = 0;
params.inputfilter.symmetric = 1;
