addpath('../src/RUSTICO/');
addpath('../src/utils/');

%% Parameters (see [1] for details)
rho = 25;%16;
sigma = 6;%3.1;
sigma0 = 2;
alpha = 0.8;
xi = 1.5;
lambda = 2;

numoriens = 8;

%% Configuration of RUSTICO
x = 101; y = 101; % center
line1(:, :) = zeros(201);
line1(:, x) = 1; %prototype line

rustico = cell(1);
params = config_params;

% Basic COSFIRE parameters
params.inputfilter.DoG.sigmalist    = sigma;
params.COSFIRE.rholist              = 0:2:rho;
params.COSFIRE.sigma0               = sigma0 / 6;
params.COSFIRE.alpha                = alpha / 6;
% Inhibition
params.RUSTICO.xi                   = xi;       % Inihbition factor
params.RUSTICO.lambda               = lambda;   % Standard deviation factor

% Orientations
params.invariance.rotation.psilist = 0:pi/numoriens:pi-pi/numoriens;

% Configuration
filterset = configureRUSTICO(line1, round([y x]), params);

%%
list_images = '/home/javi/branch-segmenter/datasets/ROSeS_depth_4_types/ORIGINAL/list_of_left_images.txt';
list_seg = '/home/javi/branch-segmenter/datasets/ROSeS_depth_4_types/ORIGINAL/list_of_left_seg_images.txt';
slash_index = find(list_images == '/', 2, 'last');
main_dir = list_images(1:slash_index(2)-1);
rustico_seg_dir = list_images(1:slash_index(1)-1);
rustico_seg_dir = sprintf('%s/%s', rustico_seg_dir, 'RUSTICO_output');
if ~exist(rustico_seg_dir, 'dir')
    mkdir(rustico_seg_dir)
end
%%
name_seg_list = get_file_list(list_seg, main_dir);
name_img_list = get_file_list(list_images, main_dir);

for i=1:length(name_img_list)
    I = imread(name_img_list{i});
    I_index = find(name_img_list{i} == '/', 3, 'last');
    I_dir = name_img_list{i}(I_index(1)+1:I_index(2)-1);
    I_name = name_img_list{i}(I_index(3)+1:end);
    I_dir = fullfile(rustico_seg_dir,I_dir);
    if ~exist(I_dir,'dir')
        mkdir(I_dir)
    end
    I = rgb2gray(I);
    I = double(I) ./ 255;
    I = imcomplement(I);
    
    seg = imread(name_seg_list{i});
    seg = rgb2gray(seg);
    seg = double(seg) ./ 255;
    seg(seg > 0) = 1;
    %figure; imagesc(I); colormap gray; axis off; axis image;
    
    %% Application of RUSTICO
    % Pad input image to avoid border effects
    NP = 50;
    I = padarray(I, [NP NP], 'replicate');
    % Filter response
    tuple = computeTuples(I, filterset);
    [response, rotations] = applyRUSTICO(I, filterset, params.RUSTICO.xi, tuple);
    response = response{1};
    response = response(NP+1:end-NP, NP+1:end-NP);
    % Save the orientation responses (use of the orientation map)
    rotations_final = zeros(size(response, 1), size(response, 2), size(rotations, 3));
    for j = 1:size(rotations, 3)
        rotations_final(:, :, j) = rotations(NP+1:end-NP, NP+1:end-NP, j);
    end
    
    % Evaluation
    perf_threshold = 49; % final threshold on Crack_PV14 data set (corresponding to 0.49 in [0,1])
    thresholds = 0.27;% 0.01:0.01:0.99;
    bestseg = [];
    f_score = 0;
    for j = 1:numel(thresholds)
        binImg = binarize(rotations_final, thresholds(j));
        binImg2 = bwmorph(binImg, 'close');
        %imshow(binImg2.*seg)
        imwrite(binImg2.*seg, fullfile(I_dir,I_name))
        %res = evalperf_1pxline(binImg2, GT, 0, 2);
        %RESULTS(j, :) = [res.Precision, res.Recall, res.FMeasure];
        binImg = [];
        binImg2 = [];
    end
    
end
fclose(fileID);


%%
function img_txt= get_file_list(f_name, main_dir)
fileID = fopen(f_name,'r');
img_txt = {};
count = 1;
while ~feof(fileID)
    img_txt{count} = sprintf('%s/%s', main_dir, fgetl(fileID));
    count=count+1;
    %disp(img_txt)
end
fclose(fileID);
end